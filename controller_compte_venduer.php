<?php

/* 
 *CONTROLLER COMTE venduer
 * 
 * //Creation ou selection compte client
 */

include_once 'lib/init.php';

$user = new user();
$produits = new produits();
if(RoleUser() !== "venduer"){
    header("Location: 404.php");
}else{
 $liste = $user->listeClient("client");
 $liste_produits = $produits->liste();
 
 include 'templates/pages/page_venduer.php';
}


