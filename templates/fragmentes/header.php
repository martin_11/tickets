<?php

/* 
 Templates de header
 * 
 */
?>

<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Tickets</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <header class="flex justify-content-around align-items-center">
            <nav>
                <a href="index.php">LOGO</a>
            </nav>
            <?php if(ifConnecter()){
                include 'templates/fragmentes/header_connect.php';
            }else{
                include 'templates/fragmentes/header_nonConnect.php';
            } ?>
            
         
          
            
        </header>