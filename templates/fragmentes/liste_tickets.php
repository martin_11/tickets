<?php

/* 
Fragments de tiktes 
 * 
 */
include 'templates/fragmentes/header.php';
?>







<div class="container">
<?php if(!$liste){ ?>
    <div class="shadow text-center">
       <div class="alert alert-secondary" role="alert">
            Votre liste de tickets est vide
    </div>
    </div>
<?php } else{ ?>
      <div class="row">
          <?php foreach ($liste as $id=>$tiket){ ?>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
              <p> <?= htmlentities($tiket->get("produits")->get("libelle")) ?></p>
            <div class="card-body">
              <?php if($tiket->get("status")==="ouvert"){ ?>
    <p style="color:#999900" class="price">Status: <?= htmlentities($tiket->get("status")) ?></p>
    <?php }elseif($tiket->get("status")==="en cours de traitment"){ ?>
     <p style="color:red" class="price">Status: <?= htmlentities($tiket->get("status")) ?></p>
    <?php }elseif($tiket->get("status")==="resolu") { ?>
             <p style="color:green" class="price">Status: <?= htmlentities($tiket->get("status")) ?></p>
         <?php } ?>
              
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group" style="margin: 0 auto">
                    <a  href="controller_detaille_ticket.php?id=<?= htmlentities( $id ) ?>&id_prod=<?= $tiket->get("produits")->get("id") ?>"><button>Voir detaille</button></a>
                  
                </div>
                
              </div>
            </div>
          </div>
        </div>
          <?php } ?>
       
      </div>
    </div>
  


<?php } ?>

<?php include 'templates/fragmentes/footer.php'; ?>