<?php

/*
 * Fragments les meeages
 */
?>

<div class="chat_liste">
<div class="chat">
<?php foreach ($liste as $demand){ ?>
   <div class="container_chat">
       <?php if($demand->get("user")->get("role") === "tech"){ ?>
       <span>Technicien : <i>"<?= htmlentities($demand->get("user")->get("nom")) ?>"</i></span>
      <?php }else{ ?>
          <span><?= htmlentities($demand->get("user")->get("nom")) ?></span>
      <?php } ?>
       
  <p><?= htmlentities($demand->get("text")) ?></p>
  <span class="time-right"><?= htmlentities($demand->get("date_message")) ?></span>
  
</div>
<?php } ?>
</div>
</div>