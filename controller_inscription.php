<?php

/* 
 *CONTROLLER INSCRIPOTION
 * method post
 * nom
 * prenom
 * email
 * password
 */


include 'lib/init.php';

$password = empty($_POST["password"]) ? "" : $_POST["password"];

//password crypter
$hashe = password_hash($password, 1);

$user = new user();

$user->setFromTab($_POST);
$user->set("role", "client");
$user->set("password", $hashe);


$user->insert();


header("Location: index.php");

