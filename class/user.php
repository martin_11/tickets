<?php

/*
 Class user
 *
 * @author élève
 */
class user extends principal {
    protected $pk = "id"; //Cles primair
    protected $table = "user";  //table du classe
    protected $champs = ["id", "nom", "prenom","email", "password", "role"];  
    
    
    
    public function verifConnect($email, $password){
        //role verifier le mail er password
        //paramtres $mail -email , $password = password
        //retpur true ou false
        $sql = "SELECT * FROM `{$this->table}` WHERE `email`=:email";
        $req = BDDselect($sql, [":email"=>$email]);
        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        if($ligne===false){
            return false;
        }
        if(password_verify($password, $ligne["password"])){
            $this->setFromTab($ligne);
            return true;
        }else{
            return false;
        }
    }
    
    
    
        public function listeClient($role){
        //Role affiche le liste des clients
        //paramtres $role role du client
        //retour tableu d'objet
        
       
        
        $sql ="SELECT * FROM `{$this->table}` WHERE `role`=:role";
        $param = [":role"=>$role];
        $req = BDDselect($sql, $param);
        
        //tableu vide
        $result = [];
        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $user = new user();
            $user->setFromTab($ligne);
            $result[$ligne["id"]] = $user;
        }
        
        return $result;
    }
    
    
    public function rechercheClient($client, $role){
        //role recher client
        //paramtres $client ->client recherche
        //retour tableu resulmt
        $sql = "SELECT * FROM `{$this->table}` WHERE `email` LIKE :client OR `nom` LIKE :client OR `prenom` LIKE :client AND `role` =:role";
        $param = [":client"=>"%$client%", ":role"=>$role];
        $req = BDDselect($sql, $param);
        $result = [];
        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $user = new user();
            $user->setFromTab($ligne);
            $result[$ligne["id"]] = $user;
        }
        return $result;
    }
    
    
}
