<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ticket
 *
 * @author élève
 */
class ticket extends principal {
    protected $pk = "id"; //Cles primair
    protected $table = "ticket";  //table du classe
    protected $champs = ["id","produits", "client", "status", "technicien"];
    protected $liens =["client"=>"user", "produits"=>"produits", "technicien"=>"user"];
    
    
    
        public function listeTiket($id){
        //Role affiche le liste des produit par client
        //paramtres $limite nb de limite dans le requete
        //retour tableu de result
        
       
        
        $sql ="SELECT * FROM `{$this->table}` WHERE `client`=:id";
        $param = [":id"=>$id];
        $req = BDDselect($sql, $param);
        
        //tableu vide
        $result = [];
        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $tiket = new ticket();
            $tiket->setFromTab($ligne);
            $result[$ligne["id"]] =$tiket;
        }
        
        return $result;
    }
    
    
    
    
       public function listeTiketStatus($status){
        //Role affiche le liste des produit par status
        //paramtres $status status de tiket
        //retour tableu de result
        
       
        
        $sql ="SELECT * FROM `{$this->table}` WHERE `status`=:status";
        $param = [":status"=>$status];
        $req = BDDselect($sql, $param);
        
        //tableu vide
        $result = [];
        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $tiket = new ticket();
            $tiket->setFromTab($ligne);
            $result[$ligne["id"]] =$tiket;
        }
        
        return $result;
    }
    
    
    
    function chekProd($id_client, $id_prod){
        $sql = "SELECT * FROM `{$this->table}` WHERE `client`=:client AND `produits`=:produits";
        $param = [":client"=>$id_client, ":produits"=>$id_prod];
        $req = BDDselect($sql, $param);
        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        
        if(!$ligne){
            return FALSE;
        }else{
            return true;
        }
    }
    
}
