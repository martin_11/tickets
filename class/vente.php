<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of vente
 *
 * @author élève
 */
class vente extends principal {
    protected $pk = "id"; //Cles primair
    protected $table = "vente";  //table du classe
    protected $champs = ["id", "client", "produits", "num_serie", "date"];
    protected $liens =["client"=>"user", "produits"=>"produits"];
    
    
    
    
    
    
        public function listeProduitsParCleitn($id){
        //Role affiche le liste des produit par client
        //paramtres $id  id du client
        //retour tableu de result
        
       
        
        $sql ="SELECT * FROM `{$this->table}` WHERE `client`=:id GROUP BY `produits`";
        $param = [":id"=>$id];
        $req = BDDselect($sql, $param);
        
        //tableu vide
        $result = [];
        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $vente = new vente();
            $vente->setFromTab($ligne);
            $result[$ligne["id"]] = $vente;
        }
        
        return $result;
    }
    
    
      public function rechercheVent($text=""){
        //role recher client
        //paramtres $client ->client recherche
        //retour tableu resulmt
       $sql = "SELECT * FROM `{$this->table}` LEFT JOIN `user` ON `{$this->table}`.client = `user`.id LEFT JOIN `produits` ON `{$this->table}`.produits = `produits`.`id` WHERE `nom` LIKE :text OR `num_serie` LIKE :text OR `libelle` LIKE :text";
          
       
        $param = [":text"=>"%$text%"];
        $req = BDDselect($sql, $param);
        $result = [];
        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)){
            $result[] = $ligne;
        }
        return $result;
    }
    
}
