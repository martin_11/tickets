<?php
////COntroller qui gere le compte de tech
include_once 'lib/init.php';



if(RoleUser() !== "tech"){
    header("Location: 404.php");
}else{
 $user = new user(idUserConnecter());
 $tiket = new ticket();
 $liste = $tiket->listeTiketStatus("ouvert");
 include 'templates/pages/page_tech.php';
}
